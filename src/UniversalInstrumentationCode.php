<?php

namespace PresserFischer;

use Exception;

abstract class UniversalInstrumentationCode
{
    /**
     * Base 2 constants for use in determining UIC
     */

    public const INT_51_MAX = 2 ** 51;
    public const INT_50_MAX = 2 ** 50;
    public const INT_37_MAX = 2 ** 37;
    public const INT_32 = 32;
    public const INT_32_MAX = 2 ** self::INT_32;
    public const INT_31_MAX = 2 ** (self::INT_32 - 1);
    public const INT_19 = 19;
    public const INT_18_MAX = 2 ** (self::INT_19 - 1);
    public const INT_5 = 5;

    /**
     * Force child implementation of Instrument UIC getter
     *
     * @return integer
     */
    abstract public function getInstrumentUic(): int;

    /**
     * Force child implementation of Instrument Domain ID getter
     *
     * @return integer
     */
    abstract public function getDomainId(): int;

    /**
     * Force child implementation of Instrument Descriptor ID getter
     *
     * @return integer
     */
    abstract public function getDescriptorId(): ?int;

    /**
     * Force child implementation of left aligned descriptor toggle
     *
     * @return boolean
     */
    abstract public function isAlignedLeft(): bool;

    /**
     * Force child implementation of doubling toggle
     *
     * @return boolean
     */
    abstract public function isDoubling(): bool;

    /**
     * Force child implementation of ordinal number getter
     *
     * @return integer|null
     */
    abstract public function getOrdinalNumber(): ?int;

    /**
     * Force child implementation of exception message for non 64-bit systems
     *
     * @return string
     */
    abstract public function getExceptionMessage(): string;

    /**
     * Long int UIC
     *
     * @return void
     * @throws Exception
     */
    public function getUICVal(): int
    {
        /**
         * Throw exception for non 64-bit PHP
         */
        if (PHP_INT_SIZE < 8) {
            throw new Exception($this->getExceptionMessage());
        }

        if ($instUic = $this->getInstrumentUic()) {
            $nres = $instUic;
            if ($domainId = $this->getDomainId()) {
                $nres += $domainId * self::INT_51_MAX;
            }

            if ($descriptorId = $this->getDescriptorId()) {
                $nres += $descriptorId * self::INT_37_MAX;
                if ($this->isAlignedLeft()) {
                    $nres += self::INT_50_MAX;
                }
            }

            $nV = (int) $this->getOrdinalNumber();
            if ($nV > 0 && $nV < self::INT_32 - 1) {
                $nres += $nV * self::INT_32_MAX;
            }

            if ($this->isDoubling()) {
                $nres += self::INT_31_MAX;
            }

            return $nres;
        }

        return 0;
    }

    /**
     * Hex version of UIC
     *
     * @return string
     */
    public function getUICValHex(): string
    {
        if ($instUic = $this->getInstrumentUic()) {
            $nresL = $instUic;
            if ($this->isDoubling()) {
                $nresL += self::INT_32_MAX;
            }

            $nresH = 0;
            $nV = (int) $this->getOrdinalNumber();

            if ($nV && $nV > 0 && $nV < self::INT_32 - 1) {
                $nresH = $nV;
            }

            if ($domainId = $this->getDomainId()) {
                $nresH += $domainId << self::INT_19;
            }

            if ($descriptorId = $this->getDescriptorId()) {
                $nresH += $descriptorId << self::INT_5;
                if ($this->isAlignedLeft()) {
                    $nresH += self::INT_18_MAX;
                }
            }

            return "{$this->formattedHex($nresH)}:{$this->formattedHex($nresL)}";
        }

        return '';
    }

    /**
     * Helper method to format int value to uppercase hex
     *
     * @param integer $value
     * @return string
     */
    private function formattedHex(int $value): string
    {
        return strtoupper(dechex($value));
    }
}
